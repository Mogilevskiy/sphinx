//
//  main.m
//  Sphinx
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import <Foundation/Foundation.h>

const int kAlphabetCount = 26;



int isPangram(NSString *inputString)
{
	int result = 0;

	if ([inputString isKindOfClass:[NSString class]] && inputString.length > 0)
	{
		BOOL cycleSkipped = NO;
		NSMutableDictionary *entries = [[NSMutableDictionary alloc] initWithCapacity:kAlphabetCount];
		for (long index = 0; index < inputString.length; ++index)
		{
			unichar character = [inputString characterAtIndex:index];
			if (character != ' ')
			{
				unichar lowercaseCharacter = tolower(character);
				if (!entries[@(lowercaseCharacter)])
				{
					entries[@(lowercaseCharacter)] = @(YES);
				}
			}
		}

		if (!cycleSkipped)
		{
			result = (entries.allKeys.count == kAlphabetCount);
		}
	}
	return result;
}

int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
	
	NSString *inputString1 = @"Lorem ipsum dolor sit amet consectetur!@#^&$*&%! adipisicing  ghjkla sdfbnmert zxcvqwp yuio elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur Excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum Lorem ipsum dolor sit amet sed do eiusmod tempor incididunt ut labo";

	int result1 = isPangram(inputString1);
	
	}
    return 0;
}