//
//  LSBaseApiClient.h
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LSApiClient.h"


@interface LSBaseApiClient : NSObject <LSApiClient>

@end
