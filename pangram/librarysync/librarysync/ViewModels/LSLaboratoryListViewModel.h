//
//  LSLaboratoryListViewModel.h
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import "LSBaseViewModel.h"
#import "LSLaboratoryCellViewModel.h"


@interface LSLaboratoryListViewModel : LSBaseViewModel

- (NSUInteger)countOfLaboratories;

- (LSLaboratoryCellViewModel *)viewModelForCellAtIndex:(NSUInteger)index;

@end
