//
//  LSBaseViewController.m
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import "LSBaseViewController.h"

@interface LSBaseViewController ()

@end

@implementation LSBaseViewController

- (void)awakeFromNib
{
	[self view];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self setupViews];
}

- (void)setViewModel:(LSBaseViewModel *)viewModel
{
	_viewModel = viewModel;
	[self setupValues];
	[self setupBindings];
}

- (void)setupViews
{
}

- (void)setupValues
{
}

- (void)setupBindings
{
}


@end
