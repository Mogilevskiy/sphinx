//
//  LSBaseViewController.h
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSBaseViewModel.h"


@interface LSBaseViewController : UIViewController

@property (nonatomic, strong) LSBaseViewModel *viewModel;

- (void)setupViews;
- (void)setupValues;
- (void)setupBindings;

@end
