//
//  LSLaboratoryListViewController.m
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import "LSLaboratoryListViewController.h"


NSString *const LSLaboratoryListCellIdentifier = @"LSLaboratoryListCellIdentifier";


@interface LSLaboratoryListViewController () <UITableViewDataSource, UITableViewDelegate>

@end


@implementation LSLaboratoryListViewController

@dynamic viewModel;

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.viewModel countOfLaboratories];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LSLaboratoryListCellIdentifier];
}

@end
