//
//  LSLaboratoryListViewController.h
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import "LSBaseViewController.h"
#import "LSLaboratoryListViewModel.h"


@interface LSLaboratoryListViewController : LSBaseViewController

@property (nonatomic, strong) LSLaboratoryListViewModel *viewModel;

@end
