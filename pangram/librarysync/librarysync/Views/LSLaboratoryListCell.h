//
//  LSLaboratoryListCell.h
//  librarysync
//
//  Created by Andrey on 16.06.15.
//  Copyright (c) 2015 Andrey Mogilevskiy. All rights reserved.
//

#import "LSBaseTableViewCell.h"
#import "LSLaboratoryCellViewModel.h"


@interface LSLaboratoryListCell : LSBaseTableViewCell

@property (nonatomic, strong) LSLaboratoryCellViewModel *viewModel;

@end
